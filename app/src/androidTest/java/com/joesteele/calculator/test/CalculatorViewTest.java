package com.joesteele.calculator.test;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import com.joesteele.calculator.*;
import com.joesteele.calculator.R;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class CalculatorViewTest {
  @Rule public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class, true, true);

  @Test public void testInputBlankUntilEqualsCalled() {
    onView(withId(R.id.result)).check(matches(withText("")));
    onView(withId(R.id.one)).perform(click());
    onView(withId(R.id.result)).check(matches(withText("")));
    onView(withId(R.id.plus)).perform(click());
    onView(withId(R.id.result)).check(matches(withText("")));
    onView(withId(R.id.one)).perform(click());
    onView(withId(R.id.result)).check(matches(withText("")));
    onView(withId(R.id.equals)).perform(click());
    onView(withId(R.id.result)).check(matches(withText("2.0")));
  }

  @Test public void testDelClearsResult() {
    onView(withId(R.id.result)).check(matches(withText("")));
    onView(withId(R.id.one)).perform(click());
    onView(withId(R.id.equals)).perform(click());
    onView(withId(R.id.result)).check(matches(withText("1.0")));
    onView(withId(R.id.del)).perform(click());
    onView(withId(R.id.result)).check(matches(withText("")));
  }

  @Test public void testDelClearsInput() {
    onView(withId(R.id.input)).check(matches(withText("")));
    onView(withId(R.id.one)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("1")));
    onView(withId(R.id.del)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("")));
  }

  @Test public void testDigitInputs() {
    onView(withId(R.id.input)).check(matches(withText("")));
    onView(withId(R.id.one)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("1")));
    onView(withId(R.id.zero)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("10")));
    onView(withId(R.id.two)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("102")));
    onView(withId(R.id.three)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("1023")));
    onView(withId(R.id.four)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("10234")));
    onView(withId(R.id.five)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("102345")));
    onView(withId(R.id.six)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("1023456")));
    onView(withId(R.id.seven)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("10234567")));
    onView(withId(R.id.dot)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("10234567.")));
    onView(withId(R.id.eight)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("10234567.8")));
    onView(withId(R.id.nine)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("10234567.89")));
  }

  @Test public void testOperatorInputs() {
    onView(withId(R.id.input)).check(matches(withText("")));
    onView(withId(R.id.two)).perform(click());
    onView(withId(R.id.divide)).perform(click());
    onView(withId(R.id.one)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("2/1")));
    onView(withId(R.id.multiply)).perform(click());
    onView(withId(R.id.two)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("2/1*2")));
    onView(withId(R.id.minus)).perform(click());
    onView(withId(R.id.two)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("2/1*2-2")));
    onView(withId(R.id.plus)).perform(click());
    onView(withId(R.id.two)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("2/1*2-2+2")));
  }

  @Test public void testIntegration() {
    onView(withId(R.id.two)).perform(click());
    onView(withId(R.id.plus)).perform(click());
    onView(withId(R.id.three)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("2+3")));
    onView(withId(R.id.equals)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("")));
    onView(withId(R.id.result)).check(matches(withText("5.0")));

    onView(withId(R.id.two)).perform(click());
    onView(withId(R.id.minus)).perform(click());
    onView(withId(R.id.three)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("2-3")));
    onView(withId(R.id.equals)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("")));
    onView(withId(R.id.result)).check(matches(withText("-1.0")));

    onView(withId(R.id.two)).perform(click());
    onView(withId(R.id.multiply)).perform(click());
    onView(withId(R.id.three)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("2*3")));
    onView(withId(R.id.equals)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("")));
    onView(withId(R.id.result)).check(matches(withText("6.0")));

    onView(withId(R.id.six)).perform(click());
    onView(withId(R.id.divide)).perform(click());
    onView(withId(R.id.three)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("6/3")));
    onView(withId(R.id.equals)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("")));
    onView(withId(R.id.result)).check(matches(withText("2.0")));

    onView(withId(R.id.four)).perform(click());
    onView(withId(R.id.plus)).perform(click());
    onView(withId(R.id.two)).perform(click());
    onView(withId(R.id.multiply)).perform(click());
    onView(withId(R.id.four)).perform(click());
    onView(withId(R.id.minus)).perform(click());
    onView(withId(R.id.two)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("4+2*4-2")));
    onView(withId(R.id.equals)).perform(click());
    onView(withId(R.id.input)).check(matches(withText("")));
    onView(withId(R.id.result)).check(matches(withText("10.0")));
  }
}
