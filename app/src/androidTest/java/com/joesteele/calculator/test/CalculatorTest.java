package com.joesteele.calculator.test;

import com.google.common.truth.Truth;
import com.joesteele.calculator.Calculator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static com.google.common.truth.Truth.assertThat;

@RunWith(JUnit4.class)
public class CalculatorTest {
  @Test public void addition() {
    Truth.assertThat(Calculator.evaluate("2+2")).isEqualTo(4.0);
    assertThat(Calculator.evaluate("2+1")).isEqualTo(3.0);
    assertThat(Calculator.evaluate("0+17")).isEqualTo(17.0);
    assertThat(Calculator.evaluate("1000+42")).isEqualTo(1042.0);
    assertThat(Calculator.evaluate("1+2+3+4")).isEqualTo(10.0);
    assertThat(Calculator.evaluate("+1+2")).isEqualTo(3.0);
    assertThat(Calculator.evaluate("1+2+")).isEqualTo(3.0);
  }

  @Test public void subtraction() {
    assertThat(Calculator.evaluate("2-2")).isEqualTo(0.0);
    assertThat(Calculator.evaluate("2-1")).isEqualTo(1.0);
    assertThat(Calculator.evaluate("0-17")).isEqualTo(-17.0);
    assertThat(Calculator.evaluate("1000-42")).isEqualTo(958.0);
    assertThat(Calculator.evaluate("4-3-2-1")).isEqualTo(-2.0);
    assertThat(Calculator.evaluate("-1-2")).isEqualTo(-3.0);
    assertThat(Calculator.evaluate("1-2-")).isEqualTo(-1.0);
  }

  @Test public void multiplication() {
    assertThat(Calculator.evaluate("2*2")).isEqualTo(4.0);
    assertThat(Calculator.evaluate("2*1")).isEqualTo(2.0);
    assertThat(Calculator.evaluate("0*17")).isEqualTo(0.0);
    assertThat(Calculator.evaluate("1000*42")).isEqualTo(42000.0);
    assertThat(Calculator.evaluate("4*3*2*1")).isEqualTo(24.0);
    assertThat(Calculator.evaluate("*1*2")).isEqualTo(0.0); // 0 is inferred before initial '*'
    assertThat(Calculator.evaluate("1*2*")).isEqualTo(2.0);
  }

  @Test public void division() {
    assertThat(Calculator.evaluate("2/2")).isEqualTo(1.0);
    assertThat(Calculator.evaluate("2/1")).isEqualTo(2.0);
    assertThat(Calculator.evaluate("0/17")).isEqualTo(0.0);
    assertThat(Calculator.evaluate("1000/42")).isEqualTo(23.80952380952381);
    assertThat(Calculator.evaluate("4/3/2/1")).isEqualTo(0.6666666666666666);
    assertThat(Calculator.evaluate("/1/2")).isEqualTo(0.0); // 0 is inferred before initial '/'
    assertThat(Calculator.evaluate("1/2/")).isEqualTo(0.5d);
  }

  @Test public void precedence() {
    assertThat(Calculator.evaluate("4+2*4-2")).isEqualTo(10.0);
    assertThat(Calculator.evaluate("2/4+3-1")).isEqualTo(2.5);
    assertThat(Calculator.evaluate("5-2+1*4+20/5")).isEqualTo(11.0);
  }
}
