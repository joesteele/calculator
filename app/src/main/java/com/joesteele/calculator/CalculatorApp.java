package com.joesteele.calculator;

import android.app.Application;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class CalculatorApp extends Application {
  @Override public void onCreate() {
    super.onCreate();
    CalligraphyConfig.initDefault(
        new CalligraphyConfig.Builder().setDefaultFontPath("fonts/Roboto-Regular.ttf")
            .setFontAttrId(R.attr.fontPath)
            .build());
  }
}
