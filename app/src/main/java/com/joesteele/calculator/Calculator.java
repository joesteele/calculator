package com.joesteele.calculator;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.joesteele.calculator.Calculator.Operator.Divide;
import static com.joesteele.calculator.Calculator.Operator.Multiply;

public final class Calculator {
  static Pattern charPattern = Pattern.compile("(\\+|\\-|\\*|\\/|\\d+)");

  private Calculator() {} // no instances

  public static double evaluate(String expression) {
    Deque<Operator> operators = new ArrayDeque<>();
    Deque<Double> values = new ArrayDeque<>();

    // is there a leading operator?
    if (Operator.forString(String.valueOf(expression.charAt(0))) != null) {
      expression = "0" + expression;
    }

    Matcher matcher = charPattern.matcher(expression);

    // iterate groupings, adding values/operators to the stack,
    // popping them off when we get to high-precedence operators
    while (matcher.find()) {
      String match = matcher.group();
      Operator operator = Operator.forString(match);

      if (operator == null) {
        values.push(Double.valueOf(match));

        // check if we have a high-precedence operator on the stack
        if (!operators.isEmpty()
            && (operators.peek().equals(Multiply)
            || operators.peek().equals(Divide))) {
          Double b = values.pop();
          Double a = values.pop();
          values.push(operators.pop().apply(a, b));
        }
      } else {
        operators.push(operator);
      }
    }

    // we pop values/operators in fifo order so we can evaluate left to right
    // (removeLast is actually the first item we added)
    while (!operators.isEmpty() && values.size() >= 2) {
      Double a = values.removeLast();
      Double b = values.removeLast();
      Operator operator = operators.removeLast();
      values.addLast(operator.apply(a, b));
    }

    return values.pop();
  }

  public static String evaluateToString(String expression) {
    return String.valueOf(evaluate(expression));
  }

  enum Operator {
    Add("+"), Subtract("-"), Multiply("*"), Divide("/");

    String string;

    Operator(String string) {
      this.string = string;
    }

    static Operator forString(String string) {
      for (Operator operator : Operator.values()) {
        if (operator.string.equals(string)) {
          return operator;
        }
      }
      return null;
    }

    double apply(double a, double b) {
      switch (this) {
        case Add:
          return a + b;
        case Subtract:
          return a - b;
        case Multiply:
          return a * b;
        case Divide:
          return a / b;
      }
      return 0d;
    }
  }
}
