package com.joesteele.calculator;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CalculatorView extends LinearLayout {
  private StringBuilder stringBuilder = new StringBuilder(32);

  @Bind(R.id.result) TextView result;
  @Bind(R.id.input) TextView input;

  public CalculatorView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected void onFinishInflate() {
    super.onFinishInflate();
    ButterKnife.bind(this);
  }

  @OnClick({
      R.id.zero, R.id.one, R.id.two, R.id.three, R.id.four, R.id.five, R.id.six, R.id.seven,
      R.id.eight, R.id.nine, R.id.dot, R.id.equals, R.id.del, R.id.plus, R.id.minus,
      R.id.divide, R.id.multiply
  }) void onClick(Button btn) {
    switch (btn.getId()) {
      case R.id.equals:
        if (stringBuilder.length() > 0) {
          result.setText(Calculator.evaluateToString(stringBuilder.toString()));
          stringBuilder.setLength(0);
        }
        break;
      case R.id.del:
        if (stringBuilder.length() > 0) {
          stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        } else {
          result.setText(null);
        }
        break;
      default:
        stringBuilder.append(btn.getText());
        break;
    }
    input.setText(stringBuilder.toString());
  }
}
